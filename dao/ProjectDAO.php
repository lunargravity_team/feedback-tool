<?php

require_once WWW_ROOT . 'classes' . DIRECTORY_SEPARATOR . 'DatabasePDO.php';

class ProjectDAO
{
    public $pdo;

    public function __construct()
    {
        $this->pdo = DatabasePDO::getInstance();
    }

    public function getProjects()
    {
        $sql = 'SELECT * FROM projects';
        $stmt = $this->pdo->prepare($sql);
        if($stmt->execute())
        {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(!empty($result)){
                return $result;
            }
        }
    }

    public function getProjectById($id)
    {
        $sql = 'SELECT * FROM projects WHERE id=:id';
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":id",$id);
        if($stmt->execute())
        {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(!empty($result)){
                return $result[0];
            }
        }
    }

    public function getProjectByExternalId($external_id)
    {
        $sql = 'SELECT * FROM projects WHERE external_id=:external_id';
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":external_id",$external_id);
        if($stmt->execute())
        {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(!empty($result)){
                return $result[0];
            }
        }
    }

    public function getProjectByToken($token)
    {
        $sql = 'SELECT * FROM projects WHERE token=:token';
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":token",$token);
        if($stmt->execute())
        {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(!empty($result)){
                return $result;
            }
        }
    }

    public function changeProjectStatus($id, $status)
    {
        $sql = 'UPDATE projects SET feedbackStatus=:feedbackStatus WHERE id=:id';
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":id",$id);
        $stmt->bindValue(":feedbackStatus",$status);
        if($stmt->execute())
        {
        }
    }

    public function insertfeedback($external_id, $token, $projectName, $clientName, $poc, $email, $feedbackStatus)
    {
        $sql = 'INSERT INTO projects (external_id,token,projectName,clientName,poc,email,feedbackStatus) VALUES (:external_id,:token,:projectName,:clientName,:poc,:email,:feedbackStatus)';
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":external_id",$external_id);
        $stmt->bindValue(":token",$token);  
        $stmt->bindValue(":projectName",$projectName);
        $stmt->bindValue(":clientName",$clientName);
        $stmt->bindValue(":poc",$poc);
        $stmt->bindValue(":email",$email);
        $stmt->bindValue(":feedbackStatus",$feedbackStatus);
        if($stmt->execute())
        {
        }
    }

    public function deletefeedback($external_id)
    {
        $sql = 'DELETE FROM projects WHERE external_id=external_id;';
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":external_id",$external_id);
        if($stmt->execute())
        {
        }
    }
}