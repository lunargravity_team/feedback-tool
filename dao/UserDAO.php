<?php

require_once WWW_ROOT . 'classes' . DIRECTORY_SEPARATOR . 'DatabasePDO.php';

class UserDAO
{
    public $pdo;

    public function __construct()
    {
        $this->pdo = DatabasePDO::getInstance();
    }

    public function checkLogin($name,$password)
    {
        $sql = 'SELECT * FROM users WHERE name=:name AND password=:password';
        $stmt = $this->pdo->prepare($sql);
         $stmt->bindValue(":name",$name);
         $stmt->bindValue(":password",$password);
        if($stmt->execute())
        {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(!empty($result)){
                $_SESSION["isLoggedIn"] = $name;
            }
            else
            {
                header('/admin/login');
            }
        }
    }
}