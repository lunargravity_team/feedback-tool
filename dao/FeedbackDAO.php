<?php

require_once WWW_ROOT . 'classes' . DIRECTORY_SEPARATOR . 'DatabasePDO.php';

class FeedbackDAO
{
    public $pdo;

    public function __construct()
    {
        $this->pdo = DatabasePDO::getInstance();
    }

    public function getFeedbackById($id)
    {
        $sql = 'SELECT * FROM feedback WHERE project_id=:id';
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":id",$id);
        if($stmt->execute())
        {
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(!empty($result)){
                return $result;
            }
        }
    }

    public function changeFeedbackAwnser($id, $awnser)
    {
        $sql = 'UPDATE feedback SET awnser=:awnser WHERE id=:id';
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":id",$id);
        $stmt->bindValue(":awnser",$awnser);
        if($stmt->execute())
        {
        }
    }

    public function insertfeedback($projectid, $question, $questiontype)
    {
        $sql = 'INSERT INTO feedback (project_id,question,question_type) VALUES (:project_id,:question,:question_type)';
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(":project_id",$projectid);
        $stmt->bindValue(":question",$question);
        $stmt->bindValue(":question_type",$questiontype);
        if($stmt->execute())
        {
        }
    }
}