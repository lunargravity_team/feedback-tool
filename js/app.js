(function()
{
	var counter = 1;
	var validated = false;
	var id = 0;
	var editmode = false;
	var questionLength = 0;
	var insertcounter = 1;

	function showInValid(element, message) {

		element.next().html(message);
	}

	function showValid(element) {

	    element.next().html('&nbsp;');
	}

	function checkTwoCharacters() {

	    if ($(this).val().length >= 2)
	    {
	        showValid($(this));
	    }
	    else
	    {
	        showInValid($(this),'Please fill in at least 2 characters');
	    }

	}

	function givefeedback(){

		$('.range').mousedown(function(){
			var range = $(this);
			var timer = window.setInterval(function(){
				range.next().text(range.val() + " / 10");
			},50);
			$('.range').mouseup(function(){
				clearInterval(timer);
			});
		});

		$('textarea').blur(checkTwoCharacters);
        $('textarea').keyup(checkTwoCharacters);

		$('.submit').on('click',function(){

			validated = true;
		    $('.givefeedback').find('textarea').blur();
		    $('.givefeedback').find('.error').each(function(key, value)
		    {
				if($(this).html() !== '&nbsp;')
				{
					validated = false;
				}
		    });

			    console.log(validated);

		    if(validated === true)
		    {
		    	$('.question').each(function()
			 	{
			 		var question = $(this);

			 		var updatefeedback ={
	                "id": question.attr('id'),
	                "awnser": question.next().val()
			        };

		            $.ajax({
		                type: 'POST',
		                url: "/feedback/update",
		                data: updatefeedback,
		                success: function(data)
		                {
		                	console.log("feedback updated")
		                }
		            });	
					
			 	});

				var updatestatus ={
	                "project_id": $('h2').attr('id'),
	                "feedbackstatus": "done"
			    };

	            $.ajax({
	                type: 'POST',
	                url: "/feedback/status",
	                data: updatestatus,
	                success: function(data)
	                {
	                	window.location.href = "/feedback/thankyou";
	                }
	            });	
		    }
		});



	}
	
	function addquestion(){
		if(!$('.question').val())
		{
			$('.addquestion').next().html("Please fill in a question");
		}
		else
		{
			if(editmode === false)
			{
				$('.addquestion').next().html("&nbsp;");
				$('.thequestionadder').before("<p id='" + counter.toString() + "'' class='"+ $('.question_type').val() + "'>" + counter.toString() + ". "+ $('.question').val() + "</p><img class='" + counter.toString() + "' src='/images/edit.png' /><br />");
				$('.question').val("");
				counter ++;
			}
			else
			{
				$('.addquestion').next().html("&nbsp;");
				$('.editquestion').prev().prev().attr('class',$('.editquestion .question_type').val());
				$('.editquestion').prev().prev().html(id.toString() + ". "+ $('.editquestion .question').val());
				$('.question').val("");
				$('.editquestion').prev().show();
				$('.editquestion').prev().prev().show();
				$('.editquestion').remove();
				$('.thequestionadder').show();
				$('.email').show();
				$('.submit').show();
				editmode = false;
			}
		}
	}

	function sendquestions(){
		 if($('.sendfeedback p').length === 0)
		 {
			$('.addquestion').next().html("Please fill in a question");
		 }
		 else
		 {

		 	$('.sendfeedback p').each(function()
		 	{
		 		questionLength ++;
		 		console.log(questionLength);
		 	});
		 	insertfeedback();
		 }
	}

	function insertfeedback()
	{
		if (insertcounter <= questionLength)
		{
			var insertFeedback ={
	        "id": $('.submit').attr('id'),
	        "question": $('#'+insertcounter.toString()).text(),
	        "questiontype": $('#'+insertcounter.toString()).attr("class")
	        };

	        $.ajax({
	            type: 'POST',
	            url: "/feedback/insert",
	            data: insertFeedback,
                success: function(data)
                {
                	insertcounter ++;
                	insertfeedback();
                }
	        });
		}
		else
		{
			var sendFeedback ={
                "id": $('.submit').attr('id'),
                "feedbackstatus": "waiting",
                "token": $('.token').val(),
                "email": $('.email').val()
	        };

            $.ajax({
                type: 'POST',
                url: "/feedback/send",
                data: sendFeedback,
                success: function(data)
                {
                	window.location.href = "/admin/projects";
                }
            });
		}
	}


    function init(){
        if($('.sendfeedback').length === 1)
        {
        	$('body').on('click','img',function(){
				console.log("click");
				id = parseInt($(this).attr('class'));
				$(this).after("<div class='editquestion'><input class='question' name='question' type='text' placeholder='type your question' /><select class='question_type' name='question_type'><option>OPEN QUESTION</option><option>RATE QUESTION</option></select><button class='addquestion' type='button'>+</button><h3>&nbsp;</h3></div>");
				$(this).prev().hide();
				$(this).hide();
				$('.thequestionadder').hide();
				$('.email').hide();
				$('.submit').hide();
				editmode = true;
			});

        	$('.addquestion').on("click",addquestion);
        	$(document).keypress(function(e) {
			    if(e.which == 13) {
			       addquestion();
			    }
			});
        	$('.submit').on("click",sendquestions);
        }
        else if($('.givefeedback').length === 1)
        {
        	givefeedback();
        }
    }

    init();
})();


