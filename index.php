<?php

require_once 'vendor/autoload.php';
require_once 'includes/functions.php';

if (session_status() == PHP_SESSION_NONE) {
    session_start();
    trace('start session');
}

define('DS', DIRECTORY_SEPARATOR);
define('WWW_ROOT', dirname(__FILE__) . DS);

$url = $_SERVER['REQUEST_URI'];
$urlclean = explode('?', $url);


if($url === "/")
{
	$controller = "Admin";
	$action = "login";
}
else
{
	$urlChunks = explode('/', $urlclean[0]);
	$controller = ucfirst($urlChunks[1]);
	$action = $urlChunks[2];
}

$route = array('controller' => $controller, 'action' => $action);
$controllerName = $controller . 'Controller';


require_once WWW_ROOT . 'controller'. DS . $controllerName . ".php";

$controllerObj = new $controllerName();
$controllerObj->route = $route;
$controllerObj->filter();
$controllerObj->render();