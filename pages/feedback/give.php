<h2 id="<?php echo $project['id']; ?>" class="client"><?php echo $project['projectName']; ?> | <?php echo $project['clientName']; ?></h2>



<section class="givefeedback">

<?php
	if($project['feedbackStatus'] === "waiting") {

		foreach ($feedback as $question) {
			echo '<p class="question" id="' . $question['id'] . '">' . $question['question'] . '</p>';

			if ($question['question_type'] === "OPEN QUESTION") {
				echo '<textarea class="awnser" name="awnser"></textarea><label class="error">&nbsp;</label>';
			} elseif ($question['question_type'] === "RATE QUESTION") {
				echo '<input class="range awnser" type="range" min="0" max="10" value="5" /><label class="counter">5 / 10</label><label class="error">&nbsp;</label>';
			}
		}
		echo '<input class="submit" name="submit" type="submit" value="SEND" />';

	} elseif($project['feedbackStatus'] === "done") {

		echo '<p>Feedback has already been given.</p>';
	}
?>
</section>