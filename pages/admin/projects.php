<a class="navButton" href="/admin/login">LOGOUT</a>

<section class="projects">
	<ul>
		<li>PROJECT</li>
		<li>CLIENT</li>
		<li>POC</li>
		<li>FEEDBACK</li>
	</ul>

	<?php foreach ($projects as $project) {?>
		<ul>
			<li><? echo $project['projectName'] ?></li>
			<li><? echo $project['clientName'] ?></li>
			<li><? echo $project['poc'] ?></li>
			<? if($project['feedbackStatus'] === "done")
			{?>
				<a class="<? echo $project['feedbackStatus'] ?>" href="/feedback/view?id=<? echo $project['id'] ?>"><? echo $project['feedbackStatus'] ?></a>
			<?
			}
			elseif($project['feedbackStatus'] === "waiting")
			{?>
				<p class="waiting"><? echo $project['feedbackStatus'] ?></p>
			<?
			}
			elseif($project['feedbackStatus'] === "none")
			{?>
				<a class="<? echo $project['feedbackStatus'] ?>" href="/feedback/make?id=<? echo $project['id'] ?>"><? echo $project['feedbackStatus'] ?></a>
			<?
			} ?>
		</ul>

	<?} ?>
	
</section>