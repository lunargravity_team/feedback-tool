<?php

require_once WWW_ROOT . 'controller' . DS . 'AppController.php';
require_once WWW_ROOT . 'dao' . DS . 'UserDAO.php';
require_once WWW_ROOT . 'dao' . DS . 'FeedbackDAO.php';
require_once WWW_ROOT . 'dao' . DS . 'ProjectDAO.php';



class AdminController extends AppController {


	public function __construct() {
		parent::__construct();
	}

    public function login(){
        unset($_SESSION["isLoggedIn"]);
    }

    public function projects(){

            if(isset($_SESSION["isLoggedIn"]))
            {
                $this->updateDB();
                $projectDAO = new ProjectDAO();
                $result = $projectDAO->getProjects();
                $this->set('projects',$result);
            }
            else
            {   
                if(isset($_POST['cosmonaut']))
                {
                    $salt = bin2hex(md5($_POST['secret']));
                    $secure_password = crypt($_POST['secret'], "Lun4rGr4vity". $salt);

                    $userDAO = new UserDAO();
                    $userDAO->checkLogin($_POST['cosmonaut'],$secure_password);
                    $this->updateDB();
                    $projectDAO = new ProjectDAO();
                    $result = $projectDAO->getProjects();
                    $this->set('projects',$result);
                }
                else
                {
                    header('Location: /admin/login',302);
                    exit;
                }
            }
    }

    public function updateDB(){
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "http://houston.lunargravity.be/api/projects");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        curl_close($curl);

        $projects = get_object_vars(json_decode($result));

        for ($i=0; $i < sizeof($projects['projects']); $i++) {
            $project = get_object_vars($projects['projects'][$i]);

            $projectDAO = new ProjectDAO();
            $result = $projectDAO->getProjectByExternalId($project['id']);

            if(empty($result))
            {
                //PROJECT IS NOT IN DB, ADD IT TO DB IF NOT ARCHIVED
                if($project['client'] === null)
                {
                    $project['client'] = "X";
                }
                if($project['status'] != "archived")
                {
                    $projectDAO->insertfeedback($project['id'], md5($project['title']), $project['title'], $project['client'], "X", "X", "none");
                }
            }
            else if($project['status'] === "archived")
            {
                //PROJECT IS IN DB, BUT THE UPDATED VERSION IS ARCHIVED, SO DELETE IT FROM DB
                $projectDAO->deletefeedback($project['id']);
            }
        }
    }
}
