<?php

require_once WWW_ROOT . 'controller' . DS . 'AppController.php';
require_once WWW_ROOT . 'dao' . DS . 'FeedbackDAO.php';
require_once WWW_ROOT . 'dao' . DS . 'ProjectDAO.php';
require_once WWW_ROOT . 'classes' . DS . 'Config.php';



class FeedbackController extends AppController {


	public function __construct() {
		parent::__construct();
	}

    public function view(){
        $feedbackDAO = new FeedbackDAO();
        $result = $feedbackDAO->getFeedbackById($_GET['id']);
        $this->set('feedback',$result);
    }

    public function make(){
        $projectDAO = new ProjectDAO();
        $project = $projectDAO->getProjectById($_GET['id']);
        $this->set('project',$project);
    }

    public function insert(){
        $feedbackDAO = new FeedbackDAO();
        $feedbackDAO->insertfeedback($_POST['id'], $_POST['question'], $_POST['questiontype']);
    }

    public function send(){
        
        
        $projectDAO = new ProjectDAO();
        $projectDAO->changeProjectStatus($_POST['id'], $_POST['feedbackstatus']);

        $mandrill = new Mandrill("viVwsD18NAQ4VtT6BcFjlQ"); 
        $message = array(
            'subject' => 'Lunar Gravity feedback',
            'from_email' => $_POST['email'],
            'html' => '<p>Beste, <br /><br />Bedankt om samen te werken met Lunar gravity. Graag hadden wij nog wat feedback gehad over onze samenwerking.
             U kan dit doen op onze <a href="'. Config::ROOT .'/feedback/give?token='.$_POST['token'].'">feedback tool</a><br /><br />Mvg, Lunar Gravity</p>',
            'to' => array(array('email' => 'laurens@lunargravity.be', 'name' => 'Recipient 1'))
           );
        $mandrill->messages->send($message);
    }

    public function give(){
        $projectDAO = new ProjectDAO();
        $project = $projectDAO->getProjectByToken($_GET['token']);

        $feedbackDAO = new FeedbackDAO();
        $feedback = $feedbackDAO->getFeedbackById($project[0]['id']);

        $this->set('project',$project[0]);
        $this->set('feedback',$feedback);
    }

    public function update(){
         $feedbackDAO = new FeedbackDAO();
         $feedbackDAO->changeFeedbackAwnser($_POST['id'], $_POST['awnser']);
    }

    public function status(){
        $projectDAO = new ProjectDAO();
        $projectDAO->changeProjectStatus($_POST['project_id'], $_POST['feedbackstatus']); 
    }

    public function thankyou(){
    }
}




